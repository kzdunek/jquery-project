function validateEmail(email) {
	var re = /\S+@\S+\.\S+/;
	return re.test(email);
}

function validateNames(names) {
	var re = /[A-Z][a-z]+(\s|,)[A-Z][a-z]{1,19}/;
	return re.test(names);
}

function validatePass(pass) {
	var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
	return re.test(pass);
}

function isLowerLetter(str) {
	return str.length === 1 && str.match(/[a-z]/i);
}

function isUpperLetter(str) {
	return str.length === 1 && str.match(/[A-Z]/i);
}

function isNumber(str) {
	return str.length === 1 && str.match(/[0-9]/i);
}

function passStrength(pass)
{
	var sila = 0;
	for (var i = 0, len = pass.length; i < len; i++) {
		if (i > 0) {
			if ((isLowerLetter(pass[i-1]) && isNumber(pass[i])) || 
			    (isLowerLetter(pass[i]) && isNumber(pass[i-1])))
				sila += 5;
			else if ((isUpperLetter(pass[i-1]) && isNumber(pass[i])) || 
			         (isUpperLetter(pass[i]) && isNumber(pass[i-1])))
				sila += 8;
			else if ((isUpperLetter(pass[i-1]) && isLowerLetter(pass[i])) || 
			         (isUpperLetter(pass[i]) && isLowerLetter(pass[i-1])))
				sila += 6;
			else
				sila += 2;
		} else {
			sila += 2;
		}
	}
	return sila;
}

function validate(valid, text)
{
	if (valid == "email")
		return validateEmail(text);
	else if (valid == "names")
		return validateNames(text);
	else if (valid == "password")
		return validatePass(text);
	return false;
}

function checkall() {
	var filled = true;
	$('body input').each(function() {
		if($(this).hasClass("error")) {
			filled = false;
		}
	});
	if(filled)
		$('#submitbutton').removeAttr("disabled");
	else
		$('#submitbutton').attr("disabled", true);
}

(function($) {

	var methods = {
		init : function() {
		},
		show : function(options) {
			var settings = $.extend({
				'text' : 'Podaj wartość'
			}, options);
			var inputText = settings.text;
			var tovalid = options.validate;
			$(this).val(inputText);
			$(this).addClass("normal");
			$(this).addClass("error");
			$('#submitbutton').attr("disabled", true);
			$(this).focus(function() {
				if ($(this).val() === inputText) {
					$(this).val('');
					$(this).removeClass("normal");
					$(this).addClass("error");
				}
			});
			
			$(this).bind('input', function(){
				if(tovalid == "password") {
					if (validate(tovalid, $(this).val())) {
						var sila = passStrength($(this).val());
						if (sila >= 0 && sila < 50)
							$("#sila").text("Siła hasła: słabe");
						else if (sila >= 50 && sila < 90)
							$("#sila").text("Siła hasła: średnie");
						else if (sila >= 90)
							$("#sila").text("Siła hasła: mocne");
						$(this).removeClass("error");
						$(this).addClass("normal");
						$("#info").removeClass("errormsg");
						$("#info").addClass("normalmsg");
					} else {
						$(this).removeClass("normal");
						$(this).addClass("error");
						$("#info").removeClass("normalmsg");
						$("#info").addClass("errormsg");
					}
				} else {
					if(validate(tovalid, $(this).val())) {
						$(this).removeClass("error");
						$(this).addClass("normal");
					} else {
						$(this).removeClass("normal");
						$(this).addClass("error");
					}
				}
				checkall();
			});
			
			

			$(this).blur(function() {
				if ($(this).val() === '') {
					$(this).val(inputText);
				}
			});
		},

		clear : function() {
			$(this).val = '';
		},

		update : function() {

		}
	};

	$.fn.inputFocus = function(method) {
		var args = arguments;
		return this.each(function(){
			if (methods[method]) {
				return methods[method].apply(this, Array.prototype.slice.call(
						args, 1));
			} else if (typeof method === 'object' || !method) {
				console.log("Init!");
				return methods.init.apply(this, args);
			} else {
				$.error(" Method " + method
						+ ' does not exists on jQuery.inputFocus');
			}
		});
	};
})(jQuery);